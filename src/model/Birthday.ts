export default interface Birthday {
    id?: number;
    firstName: string;
    lastName: string;
    day: number;
    month: number;
    year?: number;
}

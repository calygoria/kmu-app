export default interface HttpResponse<T> {
    headers: any;
    body: T;
}

import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'accueil',
      component: HomeView
    },
    {
      path: '/anniversaire',
      name: 'anniversaire',
      component: () => import('../views/BirthdayView.vue')
    },
    {
      path: '/rappel',
      name: 'rappel',
      component: () => import('../views/ReminderView.vue')
    }
  ]
})

export default router

import axios, { type AxiosInstance } from "axios";
import { authStore } from "./stores/AuthStore";

const store = authStore()

const apiClient: AxiosInstance = axios.create({
  baseURL: "kmu.calygoria.fr/api",
  timeout: 1000,
  headers: {
    "Content-type": "application/json",
  }
});

// Add a request interceptor
apiClient.interceptors.request.use((config) => {
  if (store.password) {
    const auth = btoa(`kmu:${store.password}`)
    config.headers.Authorization = `Basic ${auth}`;
  }
  return config;
});

export default apiClient;

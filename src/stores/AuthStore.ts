import dayjs from 'dayjs';
import 'dayjs/locale/fr';
import { defineStore } from "pinia";
import { ref, type Ref } from "vue";

dayjs.locale('fr');

export const authStore = defineStore('auth', () => {
  const password: Ref<string | undefined> = ref(undefined)

  return { password }
})

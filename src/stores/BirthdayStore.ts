import http from "@/http-common";
import type Birthday from "@/model/Birthday";
import dayjs from 'dayjs';
import 'dayjs/locale/fr';
import { defineStore } from "pinia";
import { ref, type Ref } from "vue";

dayjs.locale('fr');

export const birthdayStore = defineStore('birthday', () => {
  const birthdayList: Ref<Birthday[] | undefined> = ref(undefined)
  const birthdayToEdit: Ref<Birthday | undefined> = ref(undefined)

  function formatDate(birthday: Birthday): string {
    const date = dayjs(`${birthday.year ?? new Date().getFullYear()}${birthday.month}/${birthday.day}`);
    if (birthday.year) {
        return date.format('DD MMMM YYYY');
    }
    return date.format('DD MMMM');
  }

  function calculateAge(birthday: Birthday): string {
    if (!birthday.year) {
        return '';
    }
    const now = new Date();
    let age = now.getFullYear() - birthday.year;
    const m = now.getMonth() - birthday.month;
    if (m < 0 || (m === 0 && now.getDate() < birthday.day)) {
        age--;
    }
    return age.toString();
  }
  
  function fillBirthdayList() {
    http.get(`/birthday`).then(
      (res) => {
        birthdayList.value = res.data ?? [];
      },
      () => {
        birthdayList.value = [];
      }
    ).finally(() => {
      if (birthdayList.value) {
        const date = new Date()
        const actualMonth = date.getMonth()
        const actualDay = date.getDate()
        birthdayList.value.sort((a, b) => {
          if (a.month < b.month) {
            return -1
          } else if (a.month === b.month && a.day < b.day) {
            return -1
          } else if (a.month === b.month && a.day === b.day && a.year && b.year) {
            return a.year < b.year ? -1 : 1
          }
          return 1
        })
        const index = birthdayList.value.findIndex(birthday => birthday.month >= actualMonth && birthday.day > actualDay)
        const end = birthdayList.value.slice(0, index)
        const start = birthdayList.value.slice(index, birthdayList.value.length)
        birthdayList.value = [...start, ...end]
      }
    });
  }

  function createBirthday(birthdayData: Birthday) {
    birthdayData.month = parseInt(birthdayData.month.toString())
    http.post(`/birthday`, birthdayData).then(
      () => {
        fillBirthdayList()
      },
      () => {
        console.error('Unable to create birthday :(')
      }
    )
  }

  function editBirthday(birthdayData: Birthday) {
    birthdayData.month = parseInt(birthdayData.month.toString())
    http.put(`/birthday/${birthdayData.id}`, birthdayData).then(
      () => {
        fillBirthdayList()
        // birthdayToEdit.value = undefined
      },
      () => {
        console.error('Unable to edit birthday :(')
      }
    )
  }

  function deleteBirthday(birthday_data: Birthday) {
    http.delete(`/birthday/${birthday_data.id}`).then(
      () => {
        fillBirthdayList()
      },
      () => {
        console.error('Unable to delete birthday :(')
      }
    )
  }
      
  return { birthdayList, birthdayToEdit, formatDate, calculateAge, fillBirthdayList, createBirthday, editBirthday, deleteBirthday }
})
